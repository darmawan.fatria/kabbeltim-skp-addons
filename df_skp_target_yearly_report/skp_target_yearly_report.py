import time
from openerp.osv import fields, osv

class skp_target_yearly_report(osv.osv_memory):
    _name = "skp.target.yearly.report"
    
    _columns = {
        'period_year'       : fields.char('Periode Tahun', size=4, required=True),
        'print_date'       : fields.date('Tanggal Pembuatan',  required=True),
        'user_id'        : fields.many2one('res.users', 'Pejabat Yang DInilai'),
    } 
    _defaults = {
        'period_year':lambda *args: time.strftime('%Y'),
        'print_date': lambda *args: time.strftime('%Y-%m-%d'),
        'user_id': lambda self, cr, uid, ctx: uid,
    }
    
    def print_report(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        datas = {'ids': context.get('active_ids', [])}

        res = self.read(cr, uid, ids, context=context)
        res = res and res[0] or {}
        datas.update({'form': res})
        return self.pool['report'].get_action(cr, uid, ids, 
                        'df_skp_target_yearly_report.report_skp_target_yearly_report', 
                        data=datas, context=context)
skp_target_yearly_report()